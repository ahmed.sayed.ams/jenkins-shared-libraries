def call() {
    echo 'Increment mavenApp Version...'
    // Use triple quotes to prevent Groovy from interpreting ${} as GString
    sh 'mvn build-helper:parse-version versions:set \
    -DnewVersion=\\\${parsedVersion.majorVersion}.\\\${parsedVersion.minorVersion}.\\\${parsedVersion.nextIncrementalVersion} \
     versions:commit'
}
